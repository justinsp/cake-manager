FROM openjdk:11-jre-slim

COPY ./build/libs/cake-manager-*.jar ./

RUN rm -f *-javadoc.jar && rm -f *-sources.jar && mv cake-manager-*.jar cake-manager.jar

ENTRYPOINT ["java", "-jar", "cake-manager.jar"]

