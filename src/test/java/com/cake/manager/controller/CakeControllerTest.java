package com.cake.manager.controller;

import com.cake.manager.entity.Cake;
import com.cake.manager.model.CakeRequest;
import com.cake.manager.service.CakeService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class CakeControllerTest {

    @InjectMocks
    private CakeController underTest;

    @Mock
    private CakeService cakeService;

    private MockMvc mockMvc;

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(underTest).build();
    }

    @Test
    void shouldReturnOk_whenPayloadIsValid() throws Exception {
        CakeRequest cakeRequest = populateCake();
        when(cakeService.saveCake(cakeRequest)).thenReturn(populateCakeEntity(cakeRequest));
        MvcResult mvcResult = mockMvc
                .perform(post("/cakes")
                        .content(getContent(cakeRequest))
                        .contentType(APPLICATION_JSON)).andExpect(status().isOk())
                .andReturn();
        Cake cake = MAPPER.readValue(mvcResult.getResponse().getContentAsString(), Cake.class);
        assertEquals(1, cake.getCakeId());
    }

    @Test
    void shouldReturnInvalidRequest_whenPayloadIsInvalid() throws Exception {
        CakeRequest cakeRequest = new CakeRequest();
        mockMvc
                .perform(post("/cakes")
                        .content(getContent(cakeRequest))
                        .contentType(APPLICATION_JSON)).andExpect(status().isBadRequest());

    }

    @Test
    void shouldReturnOk_whenCakesAreRetrieved() throws Exception {
        mockMvc
                .perform(get("/cakes")
                        .contentType(APPLICATION_JSON)).andExpect(status().isOk());
    }

    private Cake populateCakeEntity(CakeRequest request) {
        return Cake.builder()
                .cakeId(1)
                .title(request.getTitle())
                .description(request.getDescription())
                .image(request.getDescription())
                .build();
    }

    private String getContent(CakeRequest cakeRequest) throws JsonProcessingException {
        return MAPPER.writeValueAsString(cakeRequest);
    }

    private CakeRequest populateCake() {
        CakeRequest cakeRequest = new CakeRequest();
        cakeRequest.setDescription("description");
        cakeRequest.setTitle("title");
        cakeRequest.setImage("image");
        return cakeRequest;
    }
}