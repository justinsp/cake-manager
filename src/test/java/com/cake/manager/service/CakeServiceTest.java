package com.cake.manager.service;

import com.cake.manager.entity.Cake;
import com.cake.manager.model.CakeRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class CakeServiceTest {

    @Autowired
    private CakeService underTest;

    @BeforeEach
    void setUp() {
    }

    @Test
    public void retrieveCakes_shouldReturn20Cakes() {
        List<Cake> cakes = underTest.retrieveCakes();
        assertEquals(20, cakes.size());
    }

    @Test
    public void saveCake_shouldSaveAndReturn21Cakes() {
        Cake cake = underTest.saveCake(populateCakeRequest());
        assertEquals(21, cake.getCakeId(), "Invalid cakeId, expected id is 21");
        List<Cake> cakes = underTest.retrieveCakes();
        assertEquals(21, cakes.size());
    }

    @Test
    public void downloadJSONFile_shouldReturnOk() {
        ResponseEntity<byte[]> responseEntity = underTest.downloadJSONFile();
        assertNotNull(responseEntity.getBody());
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    private CakeRequest populateCakeRequest() {
        CakeRequest request = new CakeRequest();
        request.setDescription("cake description");
        request.setTitle("cake title");
        request.setImage("cake image");
        return request;
    }
}