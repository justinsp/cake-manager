package com.cake.manager.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
public class CakeRequest {

    @NotBlank
    private String title;
    @NotBlank
    private String description;
    @NotBlank
    private String image;
}
