package com.cake.manager.service;

import com.cake.manager.entity.Cake;
import com.cake.manager.model.CakeRequest;
import com.cake.manager.repository.CakeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CakeService {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private final CakeRepository cakeRepository;

    public List<Cake> retrieveCakes() {
        return cakeRepository.findAll();
    }

    @SneakyThrows
    public ResponseEntity<byte[]> downloadJSONFile() {
        List<Cake> cakes = cakeRepository.findAll();
        String cakesJsonString = MAPPER.writeValueAsString(cakes);
        byte[] cakesJsonStringBytes = cakesJsonString.getBytes();
        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=cakes.json")
                .contentType(MediaType.APPLICATION_JSON)
                .contentLength(cakesJsonStringBytes.length)
                .body(cakesJsonStringBytes);
    }

    public Cake saveCake(CakeRequest request) {
        return cakeRepository.save(populateCakeEntity(request));
    }

    private Cake populateCakeEntity(CakeRequest request) {
        return Cake.builder()
                .title(request.getTitle())
                .description(request.getDescription())
                .image(request.getImage())
                .build();
    }
}
