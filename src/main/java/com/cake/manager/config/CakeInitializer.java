package com.cake.manager.config;

import com.cake.manager.entity.Cake;
import com.cake.manager.repository.CakeRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Configuration
@Slf4j
public class CakeInitializer {

    private static final String INPUT_JSON = "src/main/resources/cakes.json";
    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Bean
    public CommandLineRunner stream(CakeRepository cakeRepository) {
        return args -> {
            List<Cake> cakes = initialCakes();
            cakes.forEach(cakeRepository::save);
            log.info("Currently cakes in DB: {}", cakeRepository.count());
        };
    }

    private List<Cake> initialCakes() throws IOException {
        try (InputStream inputStream = new FileInputStream(INPUT_JSON)) {
            return MAPPER.readValue(inputStream, new TypeReference<>() {
            });
        }
    }
}
