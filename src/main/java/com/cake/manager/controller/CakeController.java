package com.cake.manager.controller;

import com.cake.manager.entity.Cake;
import com.cake.manager.model.CakeRequest;
import com.cake.manager.service.CakeService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@Slf4j
public class CakeController {

    private final CakeService cakeService;

    @GetMapping("/")
    public List<Cake> retrieveCakes() {
        log.info("Executing GET '/' endpoint to retrieve all the cakes from database");
        return cakeService.retrieveCakes();
    }

    @PostMapping("/cakes")
    public Cake saveCake(@Validated @RequestBody CakeRequest request) {
        log.info("Executing POST '/cakes' endpoint to save the cake to database");
        return cakeService.saveCake(request);
    }

    @GetMapping("/cakes")
    public ResponseEntity<byte[]> downloadJSONFile() { // requires client side app in order to download json file
        log.info("Executing GET '/cakes' endpoint to download all the cakes from database as JSON file");
        return cakeService.downloadJSONFile();
    }
}
